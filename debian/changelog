atf (0.21+git20240225+0c62cf9d569d-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 14:27:18 +0000

atf (0.21+git20240225+0c62cf9d569d-1) unstable; urgency=medium

  * New upstream snapshot.
  * libatf-c-1: Add Multi-Arch: same.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.
  * Update the homepage.
  * Update debian/watch.
  * New upstream release.
  * Swap descriptions of libatf-c++-2 and libatf-c-1 to match their contents
    (Closes: #1066989)

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 16 Mar 2024 17:31:20 +0100

atf (0.21-6+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 10:07:53 +0530

atf (0.21-6) unstable; urgency=medium

  * Re-enable kyua on all architectures now that Hurd is ready.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 18 Nov 2020 19:14:30 +0100

atf (0.21-5) unstable; urgency=medium

  * Don’t include <sys/mount.h> missing on Hurd.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 18 Nov 2020 15:09:11 +0100

atf (0.21-4) unstable; urgency=medium

  * Temporarily limit kyua to amd64 only.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 18 Nov 2020 13:53:15 +0100

atf (0.21-3) unstable; urgency=medium

  * Use kyua when available.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 04 Nov 2020 11:59:43 +0100

atf (0.21-2) unstable; urgency=medium

  [ Andrej Shadura ]
  * Don’t ship static libraries.
  * Add debian/not-installed.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Nicolas Braud-Santoni ]
  * Do not install ATF's own testsuite.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 30 Oct 2020 18:59:52 +0100

atf (0.21-1) unstable; urgency=medium

  * Initial release (Closes: #766576).

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 20 Dec 2019 14:54:18 +0100
